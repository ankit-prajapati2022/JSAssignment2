function intcom(a, b) {
    return a.value - b.value;
}

function charcom(a, b) {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    if (nameA < nameB)
        return -1;
    if (nameA > nameB)
        return 1;
    return 0;
}
class Preson {

    constructor(name, age, sex, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }

    static sort(arr, pov, method) {
        let ans = [...arr]
        if (method == 'asc')
            if (pov === 'name' || pov === 'sex')
                ans.sort((a, b) => a[pov].localeCompare(b[pov]))
            else if (pov === 'age' || pov === 'salary')
                ans.sort((a, b) => (a[pov] - b[pov]))
            else
                return "Invalid Parameter";

        else if (method == 'desc')
            if (pov === 'name' || pov === 'sex')
                ans.sort((b, a) => a[pov].localeCompare(b[pov]))
            else if (pov === 'age' || pov === 'salary')
                ans.sort((b, a) => (a[pov] - b[pov]))
            else
                return "Invalid Parameter";
        else
            return "Invalid Parameter";

        return ans
    }
}


let arrPreson = [
    new Preson('A', 19, 'F', 12),
    new Preson('B', 21, 'M', 15),
    new Preson('M', 15, 'F', 19),
    new Preson('G', 31, 'M', 14),
    new Preson('A', 29, 'F', 19),
    new Preson('T', 19, 'M', 16),
    new Preson('A', 24, 'F', 18),
    new Preson('R', 26, 'M', 11),
    new Preson('T', 31, 'F', 14),
]

const sortedArr = Preson.sort(arrPreson, 'sex', 'asc');
console.log(arrPreson)
console.log(sortedArr)
console.log("Working")